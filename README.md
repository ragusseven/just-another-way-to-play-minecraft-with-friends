# Just another way to поиграть в майнкрафт с друзьями

В нашем неидеальном мире невозможно реализовать такую простую вещь, как соединить два компьютера по беспроводной сети, которой мы все ежедневно пользуемся. Из-за недостатка ip-адресов и нежелания провайдеров ничего с этим делать (когда уже давно существует решение проблемы ага) вы не можете просто сходить через интернет к компьютеру друга, если у него нет белого выделенного ip.

Так при чём же тут майнкрафт? Оказывается, если вы хотите поиграть с другом на вашем приватном мире, то это вызывает множество сложностей.

# Собственно план

Покупаем самую дешёвую VDS, которую найдём, в идеале максимально близко к вам, разворачиваем на ней Wireguard VPN, подключаемся с того компа, где хотим хостить майнкрафт, пробрасываем на него 25565 порт с нашей виртуалки и всё.

## Почему это круто?

- Дёшево. Цены стартуют от 130р/мес, а если вы прошареный юзер амазона
   или azure, то возможно и бесплатно.
- Ещё дешевле. Если ваша компания играет не 24/7, то можно подключить к vpn хоста другой компании и выделить ему другой порт за половину стоимости.
- Не меняет user experience клиентов. Вашим друзьям нужно просто ввести ip или домен
   вашей виртуалки, как и любого другого майнкрафт сервера.
- Контроль. Всё полностью происходит на вашей машине. Вы можете скачать мир,
   поставить моды и тд.

## Ограничения чтоб их

- Придётся поковыряться в консоли по ssh. 
- Но собственно что нам, в первый раз что-ли.
- Не always online. Вряд ли вы хотите хостить сервер майна на вашем домашнем компе 24/7. Но ничего не мешает вам сделать это.

# Итак погнали

## Шаг 1. Покупаем VDS

Если вы не можете справиться с этим шагом самостоятельно, то тут наши дороги расходятся. Идите думайте как оплатить Exaroton из России и не думайте о таких страшных вещах. По итогам данного шага у вас должна быть куплена VDS с белым ipv4 адресом и доступом к ней по ssh.

## Шаг 2. Поднимаем Wireguard

Поднимать будем [скриптом](https://github.com/angristan/wireguard-install), с ним вы тоже должны быть в состоянии справится. После этого в вашей домашней папке появится конфигурационный файл для вашего клиента, его надо доставить на ваш будущий майнкрафт сервер и подключить его к VPN.

Так же Wireguard создаст сетевой интерфейс, название которого вам понадобится дальше, его можно найти в выводе команды ip addr.

## Шаг 3. Пробрасываем порт

Для проброса портов есть ещё один [скрипт](https://github.com/necsay/Wireguard-Forward), берём из репозитория файл `forward.sh`, запускаем его от рут пользователя на виртуалке, он задаст несколько вопросов, на них вы должны быть в состоянии ответить. 

Один из вопросов это локальный ip клиента, на который мы хотим бросить порт. Это должен быть ваш сервер майнкрафт. Как узнать локальный ip клиента? Можно подключиться к вашему vpn с клиента, после чего найти в выводе команды `ip addr` wireguard интерфейс и там будет нужный ip. Этот же ip будет в конфигурационном файле для клиента.

Второй вопрос это порт, выбирать лучше `25565`, тк он дефолтный для майнкрафта.

И да, я предполагаю, что у вас на всех машинах нормальная операционная система, а не Windows.

Скрипт, который я оставил выше, пропишет в ваш серверный конфиг wireguard обращения к `iptables`, которые заставят его перенаправлять пакеты.

Можно ли запускать скрипт несколько раз, чтобы пробросить несколько портов? Понятия не имею, попробуйте. А вообще можно сделать всё руками, отредактировав конфиг сервера `/etc/wireguard/имя_вашего_интерфейса_wireguard.conf` и перезапустить его через `wg-quick`.

## Шаг 4. Запускаем Minecraft сервер

Я буду юзать простой способ, для него нужно установить мод, который позволит открыть мир для сети на определённом порту (том который вы пробросили), найдите сами.

Как альтернативу можно использовать именно minecraft server с определённым ядром, тоже вариант.

## Шаг 4. Подключаемся

Тут всё просто, ваши друзья заходят на ваш сервер по ip виртуалки.

# Аддоны

## Хочу чтобы через VPN шёл только майнкрафт трафик

Открываем клиентский wireguard конфиг и там в поле AllowedIps пишем вместо нулей ip вашей VPN сети и маску.

## Хочу пошерить свою VPS с такой же компанией друзей (не тестировалось)

Вам нужно сгенерировать конфиг для второго клиента wireguard и пробросить для него ещё один порт. Возможно, это можно сделать скриптом, который я использовал. Помимо этого, нужно каким-то образом запретить этому человеку использовать ваш VPN для своих тёмных дел, то есть запретить ему ходить куда-то, кроме VPN сети. Это можно сделать client-side с помощью предыдущего пункта, но лучше настроить iptables на стороне сервера, чтобы он не отредактировал конфиг обратно. Не знаю как, но возможно [так](https://www.reddit.com/r/WireGuard/comments/eslim3/route_only_local_lan_traffic_through_wireguard/). Впрочем, если вы доверяете тому, кому дадите свой VPN, то это и не нужно.

# Альтернативы

## Exaroton и Aternos

Самая крутая альтернатива, на мой взгляд. За первый придётся заплатить, но совсем немного, а второй точно такой же, но бесплатный и с очередями. Тарификация происходит по времени активности сервера, то есть сервер не всегда онлайн.

Можно поставить моды и закинуть туда свой мир, есть бекапы на гугл диск. Очень удобный интерфейс и много настроек.

Но оплатить из России сейчас нельзя.

## Купить хостинг

Примеров не будет, таких ребят полно, предлагают услуги разной всратости, есть тарификация по слотам, но покупать надо от пары десятков, из-за чего стоимость может быть пару тысяч в месяц.

## Запустить сервер на VDS

Можно купить сервак помощнее, поковыряться в консоли и запустить сервер на ней. Однако чтобы играть с модами придётся купить что-то довольно мощное, я пытался хостить ванильный майн на виртуалке за 400р, было грустно даже втроём.

## Хамачи

Норм способ, но он заставляет клиентов подключаться к Hamachi, что сложнее, чем просто сходить на сервер

## VPN

Можно поднять VPN, подключить к ней всех игроков и играть как в локальной сети. Дёшево, но это увеличивает количество людей, которые подключаются к вашему VPN и надо точно знать, что они не будут творить дичь через ваш VPN или ограничить им эту возможность, об этом было выше. А ещё всем вашим друзьям придётся разобраться с тем, как подключиться к Wireguard

# Вместо итога

У этой статьи нет подробных инструкций и много чего ещё, но есть идея. Если мне было не лень, то вы читаете это на Gitlab, а значит можете предлагать свои MR, дополняя статью.